#Instructions to use the App1

1. Create .env file
    - Insert following:
    ```
        RANDOM_BASE_URL="https://api.random.org/"
        RANDOM_API_KEY="YOUR_API_KEY"
        REDIS_HOST=127.0.0.1
        REDIS_PASSWORD=
        REDIS_PORT=6379
    ```
2. Run ```composer install```
3. Create virtual host **pokerstars.app1.local**
4. Run **nginx**/**apache** with **php** enabled on root