<?php

namespace App\Console\Commands;

use Framework\Application;
use Framework\Connections\Redis;
use App\Http\Client\ApiRequest;
use React\EventLoop\Factory;

class GetRandomIntegersFromAPI
{

	private $app;

	protected $arguments = [];
	protected $loop;
	protected $redisClient;

	protected $usersLeaderboard = [
		"Casey" => 0,
	    "Austin" => 0,
	    "Tony" => 0,
	    "Raphael"=> 0,
	    "Robin" => 0,
	    "Matthew" => 0,
	    "Jessie" => 0,
	    "Aaron" => 0,
	    "Jake" => 0,
	    "Frank" => 0
	];

	protected $redisChannel = 'highscore_channel';

	/**
	 * GetRandomIntegersFromAPI constructor.
	 *
	 * @param Application $app
	 * @param $arguments
	 */
	public function __construct( Application $app, $arguments)
	{
		$this->app = $app;
		$this->arguments = $arguments;

		$this->loop = Factory::create();
		$this->redisClient = $this->app->getConnections(Redis::class)->initialize()->getClient();

		$this->loop->addPeriodicTimer(1, function () {
			$this->execute();
		});

		$this->loop->run();
	}

	/**
	 *
	 */
	public function execute(){

//		$apiRequest = []; //Used for test propose only
//		$apiRequest['result']['random']['data'] = [1,1,1,1,1,1,1,1,1,1]; //Used for test propose only

		$apiRequest = ApiRequest::request('/json-rpc/2/invoke',[
			'jsonrpc'=> '2.0',
			'method'=> 'generateIntegers',
			'params' =>[
				'apiKey' => $_ENV['RANDOM_API_KEY'],
				'n' => 10,
				'min' => 1,
				'max' => 10,
				'replacement' => true,
				'base' => 10
			],
			'id' => 1
		]);

		if(count($apiRequest) > 0 && $apiRequest['result']){
			$data = $apiRequest['result']['random']['data'];

			$index = 0;
			$this->redisClient->multi();
			foreach($this->usersLeaderboard as $key=>$val) {
				$this->redisClient->zadd( 'highscore', "INCR", [$key=>$data[$index]] );
				$index++;
			}
			$this->redisClient->exec();
			$range = $this->redisClient->zrange('highscore',0,-1,array( 'withscores'  => true));
			$this->redisClient->publish($this->redisChannel, json_encode($range));
		}
	}

	/**
	 * @return string
	 */
	public static function getClassName():string
	{
		return substr(strrchr(__CLASS__, "\\"), 1);
	}
}