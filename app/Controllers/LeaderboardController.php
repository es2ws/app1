<?php

namespace App\Controllers;

use App\Console\Commands\GetRandomIntegersFromAPI;
use Framework\Application;
use Framework\Connections\Redis;
use Framework\Singleton;
use Cocur\BackgroundProcess\BackgroundProcess;

class LeaderboardController extends Singleton
{
	protected static $instance;

	private $app;
	private $redisClient;

	/**
	 * Leaderboard constructor.
	 *
	 * @param Application $app
	 */
	public function __construct( Application $app )
	{
		$this->app         = $app;
		$this->redisClient = $this->app->getConnections( Redis::class )->initialize()->getClient();

	}

	/**
	 * Check pooling process action
	 *
	 * @return string
	 */
	public function statusAction():string{
		$status = $this->getStatus();
		return $this->app->renderAsJson( 'default.json', [ 'data' => json_encode( [ 'errors' => false, 'data' =>['active' => $status] ] ) ] );
	}

	/**
	 * Start pooling process action
	 *
	 * @return string
	 */
	public function startAction(): string
	{
		$className = GetRandomIntegersFromAPI::getClassName();

		$pid = $this->redisClient->get("PID_$className");
		$status = true;

		if(!$this->processIsRunning($pid)) {
			$process = new BackgroundProcess( "php index.php $className" );
			$process->run();

			$this->redisClient->set( "PID_$className", $process->getPid() );

			return $this->app->renderAsJson( 'default.json', [ 'data' => json_encode( [ 'errors' => false, 'data' =>['active' => $status] ] ) ] );
		}

		return $this->app->renderAsJson( 'default.json', [ 'data' => json_encode( [ 'errors' => false, 'data' =>['active' => $status] ] ) ] );
	}

	/**
	 * Stop pooling process action
	 *
	 * @return string
	 */
	public function stopAction(): string
	{
		header( 'Content-Type: application/json' );
		$className = GetRandomIntegersFromAPI::getClassName();
		$pid       = $this->redisClient->get( "PID_$className" );

		if ( $pid ) {
			$process = BackgroundProcess::createFromPID( $pid );
			if ( $process->isRunning() ) {
				$process->stop();
			}
			$this->redisClient->del( "PID_$className" );
		}

		return $this->app->renderAsJson( 'default.json', [ 'data' => json_encode( [ 'errors' => false, 'data' =>['active' => false] ] ) ] );
	}

	/**
	 * @return bool
	 */
	private function getStatus()
	{
		$className = GetRandomIntegersFromAPI::getClassName();
		$pid = $this->redisClient->get("PID_$className");
		return $this->processIsRunning($pid);
	}

	/**
	 * @param $pid
	 *
	 * @return bool
	 */
	private function processIsRunning( $pid = null ): bool
	{
		if ( ! $pid ) {
			return false;
		}

		return BackgroundProcess::createFromPID( $pid )->isRunning();
	}
}