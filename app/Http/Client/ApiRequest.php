<?php

namespace App\Http\Client;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;

class ApiRequest
{
	/**
	 * The base URL for the request.
	 *
	 * @var string
	 */
	protected static $baseUrl = '';

	/**
	 * Client used for every request.
	 *
	 * @var string
	 */
	protected static $client = null;

	/**
	 * @param string $base_url
	 *
	 * @return string
	 */
	private static function setBaseUrl( $base_url = '' ): string
	{
		if ( $base_url !== ApiRequest::$baseUrl ) {
			$client = null;
		}

		return ApiRequest::$baseUrl = ( $base_url === '' ) ? $_ENV['RANDOM_BASE_URL'] : $base_url;
	}

	/**
	 * @return Client
	 */
	private static function setClient(): Client
	{
		$base_url = ( ApiRequest::$baseUrl === '' ) ? self::setBaseUrl() : ApiRequest::$baseUrl;

		return ApiRequest::$client = new Client( [ 'verify' => false, 'base_uri' => $base_url ] );
	}

	/**
	 * @param string $command
	 * @param array $payload
	 *
	 * @return array|object
	 */
	public static function request( $command = '', array $payload = [] )
	{
		$client = ( ! ApiRequest::$client ) ? ApiRequest::setClient() : ApiRequest::$client;

		try {
			$r2 = $client->post(
				$command, [
											     RequestOptions::JSON => $payload,
					                             RequestOptions::HEADERS => [
						                             'Accept'        => 'application/json'
					                             ]
				                             ]
			);

			return json_decode( $r2->getBody()->getContents(), true );
		} catch ( ClientException $e ) {
			return [];
		}

		return [];
	}
}