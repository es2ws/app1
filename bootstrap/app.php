<?php

require_once __DIR__ . '/../vendor/es2ws/framework/src/Framework/Loader.php';
require_once __DIR__ . '/../vendor/es2ws/framework/src/Framework/Application.php';

$app = new Framework\Application(
	$_ENV['APP_BASE_PATH'] ?? dirname( __DIR__ )
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
