<?php

namespace Routes;

use App\Controllers\LeaderboardController;
use Framework\Application;
use Framework\Singleton;
use Steampixel\Route;

class Web extends Singleton
{
	protected static $instance;

	public $app = null;

	/**
	 * Web constructor.
	 *
	 * @param Application $app
	 */
	public function __construct( Application $app )
	{
		$this->app = $app;
	}

	/**
	 *
	 */
	public function init()
	{
		// Our default route
		Route::add(
			'/', function () {
			echo 'Welcome To App1';
		}, 'GET'
		);

		$leaderboard = LeaderboardController::getInstance( $this->app );

//		Route for check pooling status
		Route::add( '/leaderboard/status', function() use ($leaderboard){
			echo $leaderboard->statusAction();
		}, 'GET' );

//		Route for start pooling data
		Route::add( '/leaderboard/start', function() use ($leaderboard){
			print $leaderboard->startAction();
		}, 'GET' );

//		Route for stop pooling data
		Route::add( '/leaderboard/stop', function() use ($leaderboard){
			echo $leaderboard->stopAction();
		}, 'GET' );

		// Run the router
		Route::run( '/' );
	}
}